﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using HumanBrainBox.Data.Models.Contacts;

namespace HumanBrainBox.Models.DashboardViewModels
{
    public class DashboardHomeViewModel
    {
        public IEnumerable<Contact> Contacts { get; set; }
        public ApplicationUser ApplicationUser { get; set; }
    }
}
