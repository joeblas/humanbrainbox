﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace HumanBrainBox.Migrations
{
    public partial class Navigationpropertiesaddedtocontacts : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ApplicationUserId",
                table: "Contact",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Contact_ApplicationUserId",
                table: "Contact",
                column: "ApplicationUserId");

            migrationBuilder.AddForeignKey(
                name: "FK_Contact_AspNetUsers_ApplicationUserId",
                table: "Contact",
                column: "ApplicationUserId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Contact_AspNetUsers_ApplicationUserId",
                table: "Contact");

            migrationBuilder.DropIndex(
                name: "IX_Contact_ApplicationUserId",
                table: "Contact");

            migrationBuilder.DropColumn(
                name: "ApplicationUserId",
                table: "Contact");
        }
    }
}
