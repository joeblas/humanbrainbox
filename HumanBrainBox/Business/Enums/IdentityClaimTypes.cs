﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HumanBrainBox.Business.Enums
{
    public class IdentityClaimTypes
    {
        public static readonly string ObjectIdentifier = "http://schemas.microsoft.com/identity/claims/objectidentifier";
        public static readonly string NameIdentifier = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
        public static readonly string FirstName = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname";
        public static readonly string LastName = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/surname";
        public static readonly string Name = "name";
        public static readonly string Email = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
        public static readonly string IpAddress = "ipaddr";
    }
}
