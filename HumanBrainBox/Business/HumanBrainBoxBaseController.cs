﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HumanBrainBox.Business.Enums;
using HumanBrainBox.Data;
using HumanBrainBox.Models;
using Microsoft.ApplicationInsights;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HumanBrainBox.Business
{
    public class HumanBrainBoxBaseController : Controller
    {

        protected Guid UserId { get; set; }
        protected string NameIdentifier { get; set; }
        protected string FirstName { get; set; }
        protected string LastName { get; set; }
        protected string Name { get; set; }
        protected string Email { get; set; }
        protected string Title { get; set; }

        protected ApplicationDbContext _dbContext { get; set; }
        protected IAuthorizationService _authorizationService { get; set; }
        protected UserManager<ApplicationUser> _userManager { get; set; }

    public readonly IHttpContextAccessor _iHttpContextAccessor;

		protected TelemetryClient _Telemetry = new TelemetryClient();

        public HumanBrainBoxBaseController(IHttpContextAccessor iHttpContextAccessor,
                                            ApplicationDbContext dbContext,
                                            IAuthorizationService authorizationService,
                                            UserManager<ApplicationUser> userManager)
		{
			_iHttpContextAccessor = iHttpContextAccessor;
		    _dbContext = dbContext;
		    _authorizationService = authorizationService;
		    _userManager = userManager;

		    if (iHttpContextAccessor.HttpContext.User.Identity.IsAuthenticated)
		    {
		        UserId = iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.ObjectIdentifier) != null
		            ? Guid.Parse(iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.ObjectIdentifier).Value)
		            : Guid.Empty;
		        NameIdentifier = iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.NameIdentifier) != null
                    ? iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.NameIdentifier).Value
                    : string.Empty;
		        FirstName = iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.FirstName) != null
                    ? iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.FirstName).Value
                    : string.Empty;
                LastName = iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.LastName) != null
                    ? iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.LastName).Value
                    : string.Empty;
                Name = iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.Name) != null
                    ? iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.Name).Value
                    : string.Empty;
                Email = iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.Email) != null
                    ? iHttpContextAccessor.HttpContext.User.FindFirst(IdentityClaimTypes.Email).Value
                    : string.Empty;
            }
		}

        protected HumanBrainBoxBaseController(IHttpContextAccessor iHttpContextAccessor, ApplicationDbContext dbContext, UserManager<ApplicationUser> authorizationService)
        {
            

        }
    }
}
