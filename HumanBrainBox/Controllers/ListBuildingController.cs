﻿using HumanBrainBox.Business;
using HumanBrainBox.Data;
using HumanBrainBox.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;

namespace HumanBrainBox.Controllers
{
	[Authorize]
    public class ListBuildingController : HumanBrainBoxBaseController
    {
	    public ListBuildingController(ApplicationDbContext dbContext,
		    UserManager<ApplicationUser> userManager,
		    IHttpContextAccessor httpContextAccessor) : base(httpContextAccessor, dbContext, userManager)
	    {
		    _dbContext = dbContext;
		    _userManager = userManager;

	    }

	    public IActionResult Index()
	    {
		    
		    return View();
	    }

	}
}
