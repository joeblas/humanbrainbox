﻿using System;
using HumanBrainBox.Business;
using HumanBrainBox.Data;
using HumanBrainBox.Models;
using HumanBrainBox.Models.DashboardViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace HumanBrainBox.Controllers
{
	[Authorize]
    //[Route("[controller]/[action]")]
    public class DashboardController : HumanBrainBoxBaseController
    {

	    private readonly SignInManager<ApplicationUser> _loginManager;
	    //private readonly RoleManager<ApplicationRole> _roleManager;


	    public DashboardController(ApplicationDbContext dbContext, UserManager<ApplicationUser> userManager,
		    SignInManager<ApplicationUser> loginManager, IHttpContextAccessor httpContextAccessor) :
		    base(httpContextAccessor, dbContext, userManager)
	    {
		    _dbContext = dbContext;
		    _userManager = userManager;
		    _loginManager = loginManager;

	    }

	    

		[TempData]
        public string StatusMessage { get; set; }

        [HttpGet]
        public async Task<IActionResult> Index()
        {
            var user = await _userManager.GetUserAsync(User);
            if (user == null)
            {
                throw new ApplicationException($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            var model = new DashboardHomeViewModel
            {
                ApplicationUser = user
            };

            return View(model);
        }



        #region Helpers

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError(string.Empty, error.Description);
            }
        }



        #endregion
    }
}
