﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HumanBrainBox.Data;
using HumanBrainBox.Data.Models.Contacts;
using HumanBrainBox.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace HumanBrainBox.Controllers.ViewComponents
{
    public class ContactWidgetViewComponent : ViewComponent
    {
	    private readonly ApplicationDbContext _dbContext;
	    private readonly UserManager<ApplicationUser> _userManager;
	    private readonly SignInManager<ApplicationUser> _loginManager;

		public ContactWidgetViewComponent(ApplicationDbContext dbContext, 
										UserManager<ApplicationUser> userManager,
										SignInManager<ApplicationUser> loginManager)
	    {
		    _dbContext = dbContext;
		    _userManager = userManager;
		    _loginManager = loginManager;
	    }

		public async Task<IViewComponentResult> InvokeAsync()
		{

			var contacts = await GetItemsAsync();

		    return View(contacts);

	    }

	    private Task<List<Contact>> GetItemsAsync()
	    {
		    var userId = _userManager.GetUserId(Request.HttpContext.User);

			return _dbContext.Contact.Where(x => x.OwnerID.ToString() == userId).ToListAsync();
		}


	}
}